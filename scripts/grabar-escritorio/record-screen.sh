#!/bin/sh

STATUS=$(pidof ffmpeg)
SIZE_SCREEN=$(xrandr -q --current | grep '*' | awk '{print$1}')

if test -n "$STATUS"
then
    notify-send 'Deteniendo Grabación' 'Guardado en el directorio home' --icon=screenrecorder
    pkill ffmpeg
else
    notify-send 'Grabación' 'Empezando grabación' --icon=screenrecorder
    NAME_VIDEO="Desktop $(date)"
    ffmpeg -f x11grab  -s $SIZE_SCREEN -i :0.0 -r 25 -vcodec h264  "$HOME/$NAME_VIDEO.mp4"
fi
